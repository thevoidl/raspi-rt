The Linux kernel and the preempt-rt patchfiles
change on a daily basis. It is not guaranteed
that files like `.defconfig_piX_XX` fit every
build

Therefore the patchfiles contain as less
information as neccessary
